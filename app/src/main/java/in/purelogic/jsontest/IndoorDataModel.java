package in.purelogic.jsontest;

import org.json.JSONException;
import org.json.JSONObject;
import android.support.annotation.DrawableRes;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class IndoorDataModel {

    // TODO: Declare the member variables here
    private String mTemperature;
    private String mAqi;
    private String mHumidity;
    private String mPm25 ;
    private String mCo2 ;
    private String mHcho ;
    private String mLight ;
    private String mNoise ;
//    private String mAqiDrawable;
//    private String mPm25Drawable;
    private String mTimeStamp;
   // public static Palette mPalette = new Palette();

    public String getmTimeStamp() {return mTimeStamp;}

    public String getmTemperature() {
        return mTemperature;
    }

    public String getmAqi() {
        return mAqi;
    }

    public String getmHumidity() {
        return mHumidity;
    }

    public String getmPm25() {
        return mPm25;
    }

    public String getmCo2() {
        return mCo2;
    }

    public String getmHcho() {
        return mHcho;
    }

    public String getmLight() {
        return mLight;
    }

    public String getmNoise() {
        return mNoise;
    }

//    public int getmAqiDrawable() {
//        return mAqiDrawable;
//    }
//    public int getmPm25Drawable() {
//        return mPm25Drawable;
//    }

    public IndoorDataModel(String mTemperature, String mAqi, String mHumidity, String mPm25, String mCo2,
                           String mHcho, String mLight, String mNoise, String mTimeStamp) {
        this.mTemperature = mTemperature;
        this.mAqi = mAqi;
        this.mHumidity = mHumidity;
        this.mPm25 = mPm25;
        this.mCo2 = mCo2;
        this.mHcho = mHcho;
        this.mLight = mLight;
        this.mNoise = mNoise;
       // this.mPm25Drawable = mPalette.getPm25Drawable(mPalette.getConditionPm25(this.getmPm25()));
        //this.mAqiDrawable = mPalette.getAqiDrawable(mPalette.getConditionAqi(this.getmAqi()));
        this.mTimeStamp = mTimeStamp;
    }
    public IndoorDataModel(){}

    // TODO: Create a IndoorDataModel from a JSON:
    public static IndoorDataModel fromJson(JSONObject jsonObject) {
        IndoorDataModel indoorDataModel = new IndoorDataModel();
        try {
            //JSONObject job1 = jsonObject.getJSONObject("dataObject");

           // indoorDataModel.mAqi = JsonObject.getString("dataObject");
            indoorDataModel.mAqi = jsonObject.getString("dataObject.");
//            indoorDataModel.mTemperature = jsonObject.getString("SensorNo_201_SensorData");
//            indoorDataModel.mHumidity = jsonObject.getString("SensorNo_202_SensorData");
            Log.d("result",indoorDataModel.mAqi);
//            Log.d("tempo",indoorDataModel.mTemperature);
//            Log.d("humi",indoorDataModel.mHumidity);
//            indoorDataModel.mPm25 = job1.getString("pm25");
//            indoorDataModel.mCo2 =job1.getString("co2") ;
//            indoorDataModel.mHcho = job1.getString("hcho");
//            indoorDataModel.mLight = job1.getString("light");
//            indoorDataModel.mNoise = job1.getString("noise");
//            indoorDataModel.mTimeStamp = job1.getString("time");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return indoorDataModel;
    }

}
