package in.purelogic.jsontest;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class MainActivity extends AppCompatActivity {

    String stream ;
    TextView tvResponse ;
    final String SENSOR_INDOOR_URL = "http://fastseries.net/SensorAPI/protected/getCorpDevice?SENSORID=7210069F89FD&KEY=lsdgyj";
    //"http://weiguo.airradio.cn/smart/hwmobile/smart/cloudOuter!getCorpDevice?SENSORID=7210069F89FD&KEY=lsdgyj";

    // Creating HTTP client
    HttpClient httpClient = new DefaultHttpClient();

    // Creating HTTP Post
    HttpPost httpPost = new HttpPost("http://www.example.com/login");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvResponse = findViewById(R.id.tvResponse);
        //letsDoSomeNetworkingIndoor();
        new DownloadFilesTask().execute(SENSOR_INDOOR_URL);

    }


    private class DownloadFilesTask extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                // Check the connection status
                if (urlConnection.getResponseCode() == 200) {
                    // if response code = 200 ok
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    // Read the BufferedInputStream
                    BufferedReader r = new BufferedReader(new InputStreamReader(in));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        sb.append(line);
                    }
                     stream = sb.toString();
                    try {

                        JSONObject obj = new JSONObject(stream);

                        Log.e("wegzJson1", obj.toString());

                    } catch (Throwable t) {
                        Log.e("wegzJson", "Could not parse malformed JSON: \"" + stream + "\"");
                    }

                    Log.e("wegz",stream);
                    // Disconnect the HttpURLConnection
                    urlConnection.disconnect();

                }
            } catch (Exception e) {
                e.printStackTrace();
                stream = "NA";
                return "exception"+e ;
            }

            return stream;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //tvResponse.setText(s);
        }
    }

}